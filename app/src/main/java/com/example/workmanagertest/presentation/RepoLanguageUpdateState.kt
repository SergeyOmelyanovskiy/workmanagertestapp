package com.example.workmanagertest.presentation

data class RepoLanguageUpdateState(
    val reposUpdateState: UpdateState,
    val languageUpdateState: UpdateState
) {
    val message: String
        get() = "Repos: ${reposUpdateState.message}\nLanguages: ${languageUpdateState.message}"

    val isFinished: Boolean
        get() = reposUpdateState.isFinished && languageUpdateState.isFinished
}