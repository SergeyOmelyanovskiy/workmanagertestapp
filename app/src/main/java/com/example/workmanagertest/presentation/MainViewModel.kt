package com.example.workmanagertest.presentation

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.work.WorkInfo
import com.example.workmanagertest.WorkManagerApplication
import com.example.workmanagertest.common.ResultObject
import com.example.workmanagertest.common.SingleLiveEvent
import com.example.workmanagertest.data.model.GitHubRepoData
import com.example.workmanagertest.data.model.ProgrammingLanguage
import com.example.workmanagertest.data.repository.DataRepository
import com.example.workmanagertest.data.worker.KEY_NEW_PROGRAMMING_LANGUAGES
import com.example.workmanagertest.data.worker.UpdateEnqueueHelper

class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val dataRepository: DataRepository = (application as WorkManagerApplication).dataRepository
    private val workManagerHelper: UpdateEnqueueHelper = (application as WorkManagerApplication).workManagerHelper

    private val _infoMessage = MutableLiveData<String>()
    val infoMessage: LiveData<String> = _infoMessage

    private val languagesFilter = mutableSetOf<CharSequence>()
    private val languageFilterUpdated = MutableLiveData<Unit>()

    val languages: LiveData<List<String>> = dataRepository.getLanguages().map { languages ->
        languages.forEach { languagesFilter += it.name }
        languages.map { languageToString(it) }
    }

    private val _reposList: LiveData<List<GitHubRepoData>> = dataRepository.getRepos()

    val reposList: LiveData<List<GitHubRepoData>> = MediatorLiveData<List<GitHubRepoData>>().apply {

        addSource(_reposList) { value = it.filterRepos() }

        addSource(languageFilterUpdated) { value = _reposList.value?.filterRepos() }
    }

    val newLanguages: LiveData<List<String>> = SingleLiveEvent<List<String>>().apply {
        addSource(workManagerHelper.languagesUpdateStatus) { workInfo ->
            val newLanguages = (workInfo.outputData
                .getStringArray(KEY_NEW_PROGRAMMING_LANGUAGES)
                ?.toList()
                ?: emptyList())

            addLanguageFilters(newLanguages)
            value = newLanguages
        }
    }

    val updatingState: LiveData<RepoLanguageUpdateState> = MediatorLiveData<RepoLanguageUpdateState>().apply {
        addSource(workManagerHelper.reposUpdateState) {
            val newState = it.toUpdateState()
            value = value?.copy(reposUpdateState = newState) ?: RepoLanguageUpdateState(newState, UpdateState.Idle)
            _updateStateViewVisible.value = value?.isFinished == false
        }

        addSource(workManagerHelper.languagesUpdateStatus) {
            val newState = it.toUpdateState()
            value = value?.copy(languageUpdateState = newState) ?: RepoLanguageUpdateState(UpdateState.Idle, newState)
            _updateStateViewVisible.value = value?.isFinished == false
        }
    }

    private val _updateStateViewVisible = MutableLiveData<Boolean>()
    val updateStatusViewVisible: LiveData<Boolean> = _updateStateViewVisible

    fun onScheduleUpdate() {
        workManagerHelper.cancelWork()
        val result = workManagerHelper.enqueueWork()
        if (result is ResultObject.Error) {
            _infoMessage.postValue(result.reason)
        }
    }

    fun onCancelUpdate() {
        workManagerHelper.cancelWork()
    }

    private fun languageToString(programmingLanguage: ProgrammingLanguage): String = programmingLanguage.name

    fun addLanguageFilters(filters: List<String>) {
        filters.forEach { addLanguageFilter(it) }
    }

    fun addLanguageFilter(filter: CharSequence) {
        languagesFilter += filter
        languageFilterUpdated.value = Unit
    }

    fun removeLanguageFilter(filter: CharSequence) {
        languagesFilter -= filter
        languageFilterUpdated.value = Unit
    }

    private fun List<GitHubRepoData>?.filterRepos() = this?.filter {
        it.language == null || languagesFilter.contains(it.language)
    }?.sortedBy { it.stars }
}

private fun WorkInfo.toUpdateState() = when (state) {
    WorkInfo.State.ENQUEUED, WorkInfo.State.BLOCKED -> UpdateState.Waiting
    WorkInfo.State.RUNNING -> UpdateState.Updating
    WorkInfo.State.SUCCEEDED -> UpdateState.Updated
    WorkInfo.State.FAILED -> UpdateState.Failed
    WorkInfo.State.CANCELLED -> UpdateState.Cancelled
}