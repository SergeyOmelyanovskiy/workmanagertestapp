package com.example.workmanagertest.ui

import android.os.Bundle
import android.view.View
import androidx.preference.EditTextPreference
import androidx.preference.PreferenceFragmentCompat
import com.example.workmanagertest.R
import com.example.workmanagertest.data.datasource.local.PREF_RECURRING_INTERVAL
import com.example.workmanagertest.data.datasource.local.PREF_SCHEDULE_DELAY
import com.example.workmanagertest.ui.util.IntEditTextPreference
import com.example.workmanagertest.ui.util.TitleHandler

class ScheduleSettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.schedule_preferences, rootKey)
        setUpPreferences()
    }

    private fun setUpPreferences() {
        findPreference<IntEditTextPreference>(PREF_SCHEDULE_DELAY)?.apply {
            summaryProvider = EditTextPreference.SimpleSummaryProvider.getInstance()
        }
        findPreference<IntEditTextPreference>(PREF_RECURRING_INTERVAL)?.apply {
            summaryProvider = EditTextPreference.SimpleSummaryProvider.getInstance()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? TitleHandler)?.setTitle("Settings")
    }
}