package com.example.workmanagertest.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.workmanagertest.R
import com.example.workmanagertest.ui.util.Navigator
import com.example.workmanagertest.ui.util.TitleHandler
import com.example.workmanagertest.ui.util.openFragment
import kotlinx.android.synthetic.main.activity_main.main_toolbar as mainToolbar

class MainActivity : AppCompatActivity(), TitleHandler, Navigator {

    override fun setTitle(newTitle: String) {
        title = newTitle
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpViews()
    }

    private fun setUpViews() {
        openFragment(R.id.fragment_container, MainFragment())
        setSupportActionBar(mainToolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun navigate(fragment: Fragment) {
        openFragment(R.id.fragment_container, fragment, true)
    }
}