package com.example.workmanagertest.ui.util

interface TitleHandler {
    fun setTitle(newTitle: String)
}