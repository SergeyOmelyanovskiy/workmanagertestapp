package com.example.workmanagertest.ui.util

import androidx.fragment.app.Fragment

interface Navigator {
    fun navigate(fragment: Fragment)
}