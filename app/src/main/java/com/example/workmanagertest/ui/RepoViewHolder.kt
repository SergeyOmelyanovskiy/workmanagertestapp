package com.example.workmanagertest.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.workmanagertest.R
import com.example.workmanagertest.data.model.GitHubRepoData

class RepoViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val name: TextView = view.findViewById(R.id.repo_name)
    private val description: TextView = view.findViewById(R.id.repo_description)
    private val stars: TextView = view.findViewById(R.id.repo_stars)
    private val language: TextView = view.findViewById(R.id.repo_language)
    private val forks: TextView = view.findViewById(R.id.repo_forks)

    fun bind(item: GitHubRepoData?) {
        if (item == null) {
            showDefaultData()
        } else {
            showRepoData(item)
        }
    }

    private fun showDefaultData() {
        val resources = itemView.resources
        name.text = resources.getString(R.string.loading)
        description.visibility = View.GONE
        language.visibility = View.GONE
        stars.text = resources.getString(R.string.unknown)
        forks.text = resources.getString(R.string.unknown)
    }

    private fun showRepoData(gitHubRepo: GitHubRepoData) {
        name.text = gitHubRepo.fullName

        // if the description is missing, hide the TextView
        var descriptionVisibility = View.GONE
        if (gitHubRepo.description != null) {
            description.text = gitHubRepo.description
            descriptionVisibility = View.VISIBLE
        }
        description.visibility = descriptionVisibility

        stars.text = gitHubRepo.stars.toString()
        forks.text = gitHubRepo.forks.toString()

        // if the language is missing, hide the label and the value
        var languageVisibility = View.GONE
        if (!gitHubRepo.language.isNullOrEmpty()) {
            val resources = this.itemView.context.resources
            language.text = resources.getString(R.string.language, gitHubRepo.language)
            languageVisibility = View.VISIBLE
        }
        language.visibility = languageVisibility
    }

    companion object {
        fun create(parent: ViewGroup): RepoViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.repo_view_item, parent, false)
            return RepoViewHolder(view)
        }
    }
}