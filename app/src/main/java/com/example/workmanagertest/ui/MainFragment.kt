package com.example.workmanagertest.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.size
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.workmanagertest.R
import com.example.workmanagertest.common.observe
import com.example.workmanagertest.presentation.MainViewModel
import com.example.workmanagertest.ui.util.Navigator
import com.example.workmanagertest.ui.util.TitleHandler
import com.example.workmanagertest.ui.util.toVisibleOrGone
import com.google.android.material.chip.Chip
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_main.cancel_button as cancelButton
import kotlinx.android.synthetic.main.fragment_main.languages_filter as languagesFilterView
import kotlinx.android.synthetic.main.fragment_main.repos_view as reposListView
import kotlinx.android.synthetic.main.fragment_main.update_button as updateButton
import kotlinx.android.synthetic.main.fragment_main.update_status as updateStatusView

class MainFragment : Fragment() {

    private val viewModel by viewModels<MainViewModel>()

    private val reposAdapter = RepoListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        bindToViewModel()
    }

    private fun setUpViews() {
        setTitle("Updates")
        reposListView.adapter = reposAdapter
        updateButton.setOnClickListener { viewModel.onScheduleUpdate() }

        cancelButton.setOnClickListener {
            viewModel.onCancelUpdate()
            showSnackMessage("Update CANCELLED")
        }
    }

    private fun bindToViewModel() {
        viewModel.reposList.observe(this) { reposAdapter.submitList(it) }

        viewModel.languages.observe(this) { displayChips(it) }

        viewModel.newLanguages.observe(this) { newLanguages ->
            if (newLanguages.isNotEmpty()) {
                val langs = newLanguages.joinToString(separator = ", ")
                showSnackMessage("New languages: $langs", Snackbar.LENGTH_LONG)
            }
        }

        viewModel.infoMessage.observe(this) { showSnackMessage(it) }

        viewModel.updatingState.observe(this) { updateStatusView.text = it.message }

        viewModel.updateStatusViewVisible.observe(this) { updateStatusViewVisibility(it) }
    }

    private fun updateStatusViewVisibility(show: Boolean) = updateStatusView.toVisibleOrGone(show)

    private fun displayChips(labels: List<String>) {
        languagesFilterView.removeAllViews()
        labels.forEach { label ->
            val chip = Chip(requireContext()).apply {
                text = label
                isCheckable = true
                isClickable = true
                isChecked = true
                setLanguageFilterListener()
            }
            languagesFilterView.addView(chip)
        }

        languagesFilterView.toVisibleOrGone(isVisible = languagesFilterView.size != 0)
    }

    private fun Chip.setLanguageFilterListener() = setOnCheckedChangeListener { _, isChecked ->
        if (isChecked) {
            viewModel.addLanguageFilter(text)
        } else {
            viewModel.removeLanguageFilter(text)
        }
    }

    private fun showSnackMessage(message: String, length: Int = Snackbar.LENGTH_SHORT) {
        Snackbar.make(requireView(), message, length).show()
    }

    private fun setTitle(title: String) = (activity as? TitleHandler)?.setTitle(title)

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.menu_settings -> {
            goToSettings()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun goToSettings() = (requireActivity() as? Navigator)?.navigate(ScheduleSettingsFragment())
}