package com.example.workmanagertest.data.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.workmanagertest.WorkManagerApplication
import com.example.workmanagertest.data.model.GitHubRepoData
import com.example.workmanagertest.data.model.ProgrammingLanguage
import com.example.workmanagertest.data.util.simulateLongRunningWork
import com.example.workmanagertest.ui.util.NotificationHelper

private const val NOTIFICATION_ID = 2
private const val NOTIFICATION_TITLE = "Languages update"

/**
 * Updates list of languages used throughout the GitHub repos list displayed in the app
 */
class UpdateLanguagesWorker(applicationContext: Context, parameters: WorkerParameters) :
    Worker(applicationContext, parameters) {
    private val notificationHelper = NotificationHelper(applicationContext, NOTIFICATION_TITLE, NOTIFICATION_ID)

    private val dataRepository = (applicationContext as WorkManagerApplication).dataRepository

    override fun doWork(): Result {
        notificationHelper.showStartedNotification()

        val allRepos = dataRepository.getReposSync()
        val languageNamesBeforeUpdate = dataRepository.getLanguagesSync().map { it.name }
        val newLanguagesNames = getLanguagesFromLastRepos()?.let { it - languageNamesBeforeUpdate }?.toTypedArray()
        val allLanguages = allRepos.mapToLanguages()

        simulateLongRunningWork()

        dataRepository.addLanguages(allLanguages)
        notificationHelper.showSuccessNotification()

        return Result.success(
            workDataOf(KEY_NEW_PROGRAMMING_LANGUAGES to newLanguagesNames)
        )
    }

    private fun getLanguagesFromLastRepos(): List<String>? {
        val lastReposIds = inputData.getLongArray(KEY_NEW_REPOS_IDS)
        return lastReposIds
            ?.let { dataRepository.getReposByIdSync(it.toList()) }
            ?.let { it.mapToLanguages() }
            ?.map { it.name }
    }

    private val GitHubRepoData.programmingLanguage
        get() = language?.let { name ->
            ProgrammingLanguage(name)
        }

    private fun List<GitHubRepoData>.mapToLanguages() = distinctBy { it.language }
        .mapNotNull { it.programmingLanguage }
}