package com.example.workmanagertest.data.datasource.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.workmanagertest.data.model.GitHubRepoData
import com.example.workmanagertest.data.model.Page
import com.example.workmanagertest.data.model.ProgrammingLanguage

@Database(
    entities = [GitHubRepoData::class, Page::class, ProgrammingLanguage::class],
    version = 1,
    exportSchema = false
)
abstract class GitHubDatabase : RoomDatabase() {
    abstract fun getDao(): GitHubDao
}