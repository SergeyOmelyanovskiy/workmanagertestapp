package com.example.workmanagertest.data.datasource.local

import android.app.Application
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager

const val PREF_SCHEDULE_DELAY = "SCHEDULE_DELAY"
const val PREF_REQUIRE_CHARGING = "REQUIRE_CHARGING"
const val PREF_REQUIRE_NETWORK = "REQUIRE_NETWORK"
const val PREF_IS_RECURRING = "IS_RECURRING"
const val PREF_RECURRING_INTERVAL = "RECURRING_INTERVAL"
const val PREF_RETRY_UPDATE = "RETRY_UPDATE"
const val PREF_SHOW_NOTIFICATIONS = "SHOW_NOTIFICATIONS"
const val PREF_REPOS_UPDATE_WORK_ID = "UPDATE_REPOS_WORK_ID"
const val PREF_LANGS_UPDATE_WORK_ID = "UPDATE_LANGS_WORK_ID"

private const val DEFAULT_SCHEDULE_DELAY = 0
private const val DEFAULT_UPDATE_WORK_ID = ""

class CommonStoreImpl(application: Application) : CommonStore,
    SharedPreferences by PreferenceManager.getDefaultSharedPreferences(application) {

    override var scheduleDelaySeconds: Int
        get() = getInt(PREF_SCHEDULE_DELAY, DEFAULT_SCHEDULE_DELAY)
        set(value) {
            edit {
                getInt(PREF_SCHEDULE_DELAY, value)
            }
        }

    override var requireCharging: Boolean
        get() = getBoolean(PREF_REQUIRE_CHARGING, false)
        set(value) {
            edit { putBoolean(PREF_REQUIRE_CHARGING, value) }
        }

    override var requireNetwork: Boolean
        get() = getBoolean(PREF_REQUIRE_NETWORK, false)
        set(value) {
            edit { putBoolean(PREF_REQUIRE_NETWORK, value) }
        }

    override var isRecurring: Boolean
        get() = getBoolean(PREF_IS_RECURRING, false)
        set(value) {
            edit { putBoolean(PREF_IS_RECURRING, value) }
        }

    override var repeatIntervalMinutes: Int
        get() = getInt(PREF_RECURRING_INTERVAL, 0)
        set(value) {
            edit { putInt(PREF_RECURRING_INTERVAL, value) }
        }

    override var doRetryUpdate: Boolean
        get() = getBoolean(PREF_RETRY_UPDATE, false)
        set(value) {
            edit { putBoolean(PREF_RETRY_UPDATE, value) }
        }

    override var showNotifications: Boolean
        get() = getBoolean(PREF_SHOW_NOTIFICATIONS, false)
        set(value) {
            edit { putBoolean(PREF_SHOW_NOTIFICATIONS, value) }
        }

    override var reposUpdateWorkId: String?
        get() = getString(PREF_REPOS_UPDATE_WORK_ID, DEFAULT_UPDATE_WORK_ID)
        set(value) {
            edit { putString(PREF_REPOS_UPDATE_WORK_ID, value) }
        }

    override var languagesUpdateWorkId: String?
        get() = getString(PREF_LANGS_UPDATE_WORK_ID, DEFAULT_UPDATE_WORK_ID)
        set(value) {
            edit { putString(PREF_LANGS_UPDATE_WORK_ID, value) }
        }
}