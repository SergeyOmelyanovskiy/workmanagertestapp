package com.example.workmanagertest.data.datasource.local

interface CommonStore {
    var scheduleDelaySeconds: Int
    var requireCharging: Boolean
    var requireNetwork: Boolean
    var isRecurring: Boolean
    var repeatIntervalMinutes: Int
    var doRetryUpdate: Boolean
    var showNotifications: Boolean
    var reposUpdateWorkId: String?
    var languagesUpdateWorkId: String?
}