package com.example.workmanagertest.data.datasource.local

import androidx.lifecycle.LiveData
import com.example.workmanagertest.data.model.GitHubRepoData
import com.example.workmanagertest.data.model.Page
import com.example.workmanagertest.data.model.ProgrammingLanguage

class LocalGitHubDataSource(private val gitHubDao: GitHubDao) {

    fun getLastLoadedPageNumber(): Int = gitHubDao.getLastLoadedPage()?.pageNumber ?: 0

    fun setLastLoadedPageNumber(pageNumber: Int) {
        gitHubDao.setLasLoadedPage(Page.create(pageNumber))
    }

    fun putRepos(gitHubRepos: List<GitHubRepoData>) {
        gitHubDao.putRepos(gitHubRepos)
    }

    fun getRepos(): LiveData<List<GitHubRepoData>> = gitHubDao.getRepos()

    fun getReposSync(): List<GitHubRepoData> = gitHubDao.getReposSync()

    fun getReposByIdSync(ids: List<Long>): List<GitHubRepoData> = gitHubDao.getReposByIdSync(ids)

    fun getLanguages() = gitHubDao.getLanguages()

    fun getLanguagesSync() = gitHubDao.getLanguagesSync()

    fun addLanguages(languages: List<ProgrammingLanguage>) = gitHubDao.putLanguages(languages)
}