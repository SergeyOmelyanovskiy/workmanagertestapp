package com.example.workmanagertest.data.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.workmanagertest.WorkManagerApplication
import com.example.workmanagertest.data.model.GitHubRepoData
import com.example.workmanagertest.data.model.ProgrammingLanguage
import com.example.workmanagertest.data.repository.DataRepository
import com.example.workmanagertest.data.repository.SettingsRepository
import com.example.workmanagertest.data.util.simulateLongRunningWork
import com.example.workmanagertest.ui.util.NotificationHelper

private val TAG = UpdateGithubReposWorker::class.java.simpleName

private const val NOTIFICATION_TITLE = "Repo and language update"

/**
 * Updates GitHub repos list
 */
class PeriodicWorker(applicationContext: Context, parameters: WorkerParameters) :
    Worker(applicationContext, parameters) {
    private val notificationHelper = NotificationHelper(applicationContext, NOTIFICATION_TITLE)

    private val dataRepository: DataRepository = (applicationContext as WorkManagerApplication).dataRepository
    private val settingsRepository: SettingsRepository =
        (applicationContext as WorkManagerApplication).settingsRepository

    override fun doWork(): Result {
        notificationHelper.showStartedNotification()

        val newRepos = dataRepository.updateAndGetNewRepos()
        val languageUpdateResult = getLanguages(newRepos)
        val success = newRepos != null && languageUpdateResult is Result.Success

        return when {
            success -> {
                notificationHelper.showSuccessNotification()
                Result.success()
            }
            settingsRepository.doRetryUpdate -> {
                notificationHelper.showFailureNotification()
                Result.retry()
            }
            else -> {
                notificationHelper.showFailureNotification()
                Result.failure()
            }
        }
    }

    private fun getLanguages(lastRepos: List<GitHubRepoData>?): Result {
        notificationHelper.showStartedNotification()

        val allRepos = dataRepository.getReposSync()
        val languageNamesBeforeUpdate = dataRepository.getLanguagesSync().map { it.name }
        val newLanguagesNames = getLanguagesFromLastRepos(lastRepos)?.let { it - languageNamesBeforeUpdate }?.toTypedArray()
        val allLanguages = allRepos.mapToLanguages()

        simulateLongRunningWork()

        dataRepository.addLanguages(allLanguages)
        notificationHelper.showSuccessNotification()

        return Result.success(
            workDataOf(KEY_NEW_PROGRAMMING_LANGUAGES to newLanguagesNames)
        )
    }

    private fun getLanguagesFromLastRepos(lastRepos: List<GitHubRepoData>?): List<String>? =
        lastRepos?.mapToLanguages()?.map { it.name }

    private val GitHubRepoData.programmingLanguage
        get() = language?.let { name ->
            ProgrammingLanguage(name)
        }

    private fun List<GitHubRepoData>.mapToLanguages() = distinctBy { it.language }
        .mapNotNull { it.programmingLanguage }
}