package com.example.workmanagertest.data.worker

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import androidx.work.Constraints
import androidx.work.ExistingWorkPolicy
import androidx.work.ListenableWorker
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.example.workmanagertest.common.ResultObject
import com.example.workmanagertest.data.repository.SettingsRepository
import com.example.workmanagertest.data.repository.WorkerDataRepository
import com.example.workmanagertest.data.util.enqueueUnique
import com.example.workmanagertest.data.util.setDefaultBackoffCriteria
import java.util.UUID
import java.util.concurrent.TimeUnit

private const val UNIQUE_WORKER_NAME = "updateRepos"
private const val LANGUAGE_UPDATE_DELAY_SECONDS = 0L

class UpdateEnqueueHelper(
    private val settingsRepo: SettingsRepository,
    private val workManager: WorkManager,
    private val workerDataRepository: WorkerDataRepository
) {

    private var languagesUpdateWorkId = MutableLiveData<UUID>().apply {
        workerDataRepository.reposUpdateWorkId?.toUuid { value = it }
    }
    private var reposUpdateWorkId = MutableLiveData<UUID>().apply {
        workerDataRepository.reposUpdateWorkId?.toUuid { value = it }
    }

    private val workConstraints: Constraints
        get() = Constraints.Builder()
            .setRequiresCharging(settingsRepo.requireCharging)
            .also { constraints ->
                if (settingsRepo.requireNetwork) {
                    constraints.setRequiredNetworkType(NetworkType.CONNECTED)
                }
            }
            .build()

    val languagesUpdateStatus: LiveData<WorkInfo> = languagesUpdateWorkId.switchMap {
        workManager.getWorkInfoByIdLiveData(it)
    }

    val reposUpdateState: LiveData<WorkInfo> = reposUpdateWorkId.switchMap {
        workManager.getWorkInfoByIdLiveData(it)
    }

    fun enqueueWork() = if (settingsRepo.isRecurring) {
        enqueuePeriodicWork()
        ResultObject.Success
    } else {
        enqueueReposAndLanguageUpdate()
    }

    fun cancelWork() = workManager.cancelUniqueWork(UNIQUE_WORKER_NAME)

    private fun enqueuePeriodicWork() {
        val workRequest = createPeriodicRequest<PeriodicWorker>()

        reposUpdateWorkId.value = workRequest.id
        workerDataRepository.reposUpdateWorkId = workRequest.id.toString()
        languagesUpdateWorkId.value = workRequest.id
        workerDataRepository.languagesUpdateWorkId = workRequest.id.toString()

        workManager.enqueueUnique(workRequest, UNIQUE_WORKER_NAME)
    }

    private fun enqueueReposAndLanguageUpdate(): ResultObject {
        if (settingsRepo.isRecurring) {
            return ResultObject.Error("Recurring work cannot be chained")
        }

        val updateRepoRequest = createOneTimeRequest<UpdateGithubReposWorker>()
        val updateLanguagesRequest = createOneTimeRequest<UpdateLanguagesWorker>(LANGUAGE_UPDATE_DELAY_SECONDS)

        workManager.beginUniqueWork(
            UNIQUE_WORKER_NAME,
            ExistingWorkPolicy.REPLACE,
            updateRepoRequest
        ).then(updateLanguagesRequest).enqueue()
        languagesUpdateWorkId.value = updateLanguagesRequest.id
        reposUpdateWorkId.value = updateRepoRequest.id
        return ResultObject.Success
    }

    private inline fun <reified WORKER_TYPE : ListenableWorker> createOneTimeRequest(initialDelay: Long = settingsRepo.scheduleDelaySeconds.toLong()) =
        OneTimeWorkRequestBuilder<WORKER_TYPE>()
            .setInitialDelay(initialDelay, TimeUnit.SECONDS)
            .setConstraints(workConstraints)
            .setDefaultBackoffCriteria()
            .build()

    private inline fun <reified WORKER_TYPE : ListenableWorker> createPeriodicRequest() =
        PeriodicWorkRequestBuilder<WORKER_TYPE>(
            settingsRepo.repeatIntervalMinutes.toLong(),
            TimeUnit.SECONDS
        ).setConstraints(workConstraints)
            .setDefaultBackoffCriteria()
            .build()

    private fun String.toUuid(onSuccess: (UUID) -> Unit) = try {
        onSuccess(UUID.fromString(this))
    } catch (e: Exception) {
        e.printStackTrace()
    }
}