package com.example.workmanagertest.common

sealed class ResultObject {
    object Success : ResultObject()
    data class Error(val reason: String) : ResultObject()
}